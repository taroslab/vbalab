Attribute VB_Name = "Module1"
Option Explicit

Declare Function DllArgMain Lib "DllArg.dll" (ByVal str0 As String, ByVal str1 As String) As Long

Sub StrArgMain()
    Dim exitStat As Long
    
    exitStat = DllArgMain("arg0", "arg1")
    
    MsgBox exitStat
End Sub
