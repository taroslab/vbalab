VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
END
Attribute VB_Name = "File"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Option Explicit

' ファイル選択ダイアログのファイルフィルター
Private Const COMMA As String = ","
Private Const FILE_FILTER_ALL As String = "すべてのファイル,*.*"
Private Const FILE_FILTER_TEXT As String = "テキストファイル,*.txt"
Private Const FILE_FILTER_CSV As String = "CSV,*.csv"

' TextStreamオブジェクト
Private ts As Object

'**
'* コンストラクタ
'*
Public Sub Class_Initialize()
    ' DO NOTHING
End Sub

'**
'* デストラクタ
'*
Public Sub Class_Terminate()
    ' DO NOTHING
End Sub

'**
'* ファイルを開く。
'* @param filename ファイル名
'*
Public Sub OpenFile(ByVal filename As String)
    Dim fso As Object ' ファイルシステムオブジェクト
    
    ' TextStreamを取得する
    Set fso = CreateObject("Scripting.FileSystemObject")
    Set ts = fso.OpenTextFile(filename)
    
    Set fso = Nothing
End Sub

'**
'* ファイル選択ダイアログで指定してファイルを開く。
'*
Public Sub OpenSelectFile()
    Dim fso As Object       ' ファイルシステムオブジェクト
    Dim filename As String  ' ファイル名
    
    ' ファイル選択ダイアログを表示し、ファイル名を取得する
    filename = Application.GetOpenFilename(GetFileFilters)
    
    If filename <> "False" Then
        ' ファイルオープンに成功した場合、TextStreamを取得する
        Set fso = CreateObject("Scripting.FileSystemObject")
        Set ts = fso.OpenTextFile(filename)
    End If
    
    Set fso = Nothing
End Sub

'**
'* ファイル選択ダイアログで使用するファイルフィルターを取得する。
'* @return ファイルフィルター
'*
Private Function GetFileFilters() As String
    GetFileFilters = FILE_FILTER_ALL & COMMA _
                    & FILE_FILTER_TEXT & COMMA _
                    & FILE_FILTER_CSV
End Function

'**
'* ファイルを閉じる。
'* ファイルを開いた後ファイルの使用が終了した場合、当関数をコールして必ずファイルを閉じること。
'*
Public Sub CloseFile()
    ts.Close
End Sub

'**
'* 1文字読み込む。ファイルの最終行になったとき、関数EOFの戻り値がtrueになる。
'* @return ファイルの1文字
'*
Public Function Read() As String
    Read = ts.Read(1)
End Function

'**
'* 1行読み込む。ファイルの最終行になったとき、関数EOFの戻り値がtrueになる。
'* @return ファイルの1行
'*
Public Function ReadLine() As String
    ReadLine = ts.ReadLine
End Function

'**
'* ファイルの内容をすべて読み込む。
'* @return ファイルの内容すべて
'*
Public Function ReadAll() As String
    ReadAll = ts.ReadAll
End Function

'**
'* ファイルの最終行のtrueになる。
'* @return ファイルの最終行の場合true、そうでない場合false
'*
Public Function EOF() As Boolean
    EOF = ts.AtEndOfFile
End Function
