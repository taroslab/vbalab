Attribute VB_Name = "FileAdoModule"
Option Explicit

Const OUTPUT_POS As String = "A5"

'**
'* Open Fileメイン
'*
Function OpenFileAdo() As Object
    Dim f As New FileAdo   ' ファイル
    
    ' ファイルオープン
    f.OpenSelectFile
    
    ' ファイルクローズ
    ' ファイルクローズはデストラクタ内で実施しているため、明示的にする必要なし
    
    Set f = Nothing
End Function

'**
'* Read Lileメイン
'*
Function ReadLineAdo() As Object
    Dim f As New FileAdo   ' ファイル
    Dim line As String  ' ファイルの1行
    
    ' ファイルオープン
    f.OpenSelectFile
    
    ' 1行を読み込む
    line = f.ReadLine
    
    ' セルにファイルの1行を設定
    FileAdoSheet.Range(OUTPUT_POS).Value = line
    
    ' ファイルクローズ
    ' ファイルクローズはデストラクタ内で実施しているため、明示的にする必要なし
    
    Set f = Nothing
End Function

