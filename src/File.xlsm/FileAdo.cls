VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
END
Attribute VB_Name = "FileAdo"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Option Explicit

' ファイル選択ダイアログのファイルフィルター
Private Const COMMA As String = ","
Private Const FILE_FILTER_ALL As String = "すべてのファイル,*.*"
Private Const FILE_FILTER_TEXT As String = "テキストファイル,*.txt"
Private Const FILE_FILTER_CSV As String = "CSV,*.csv"

' ADOオブジェクト
Private ado As Object

'**
'* コンストラクタ
'*
Public Sub Class_Initialize()
    ' ADOストリーム作成
    Set ado = CreateObject("ADODB.Stream")
    
    ' ファイルのモード指定
    ' 1 : バイナリモード
    ' 2 : テキストモード
    ado.Type = 2
    
    ' ファイルの文字コード指定
    ' UTF-8
    ' Shift-JIS
    ' EUC-JP
    ado.Charset = "UTF-8"
    
    ' ファイルの改行コードの指定
    ' -1 : CRLF
    ' 10 : LF
    ' 13 : CR
    ado.LineSeparator = 10
    
    ' ADOストリームオープン
    ado.Open
End Sub

'**
'* デストラクタ
'*
Public Sub Class_Terminate()
    ' ADOストリームクローズ
    ado.Close
End Sub

'**
'* ファイルを開く。
'* @param filename ファイル名
'*
Public Sub OpenFile(ByVal filename As String)
    ado.LoadFromFile filename
End Sub

'**
'* ファイル選択ダイアログで指定してファイルを開く。
'*
Public Sub OpenSelectFile()
    Dim filename As String  ' ファイル名
    
    ' ファイル選択ダイアログを表示し、ファイル名を取得する
    filename = Application.GetOpenFilename(GetFileFilters)
    
    If filename <> "False" Then
        ' ファイルオープンに成功した場合、TextStreamを取得する
        ado.LoadFromFile filename
    End If
End Sub

'**
'* ファイル選択ダイアログで使用するファイルフィルターを取得する。
'* @return ファイルフィルター
'*
Private Function GetFileFilters() As String
    GetFileFilters = FILE_FILTER_ALL & COMMA _
                    & FILE_FILTER_TEXT & COMMA _
                    & FILE_FILTER_CSV
End Function

'**
'* 1行読み込む。ファイルの最終行になったとき、関数EOFの戻り値がtrueになる。
'* @return ファイルの1行
'*
Public Function ReadLine() As String
    ' 引数に-2を設定すると1行読み込みになる
    ReadLine = ado.ReadText(-2)
End Function

'**
'* ファイルの内容をすべて読み込む。
'* @return ファイルの内容すべて
'*
Public Function ReadAll() As String
    ' 引数はデフォルトで-1のため省略しても構わない
    ' ReadAll = ado.ReadText(-1)
    ReadAll = ado.ReadText
End Function

'**
'* ファイルの最終行の場合trueになる。
'* @return ファイルの最終行の場合true、そうでない場合false
'*
Public Function EOF() As Boolean
    EOF = ado.EOS
End Function
