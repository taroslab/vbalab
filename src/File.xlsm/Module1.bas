Attribute VB_Name = "Module1"
Option Explicit

Const OUTPUT_POS As String = "A5"

'**
'* Open Fileメイン
'*
Function OpenFileMain() As Object
    Dim f As New File   ' ファイル
    
    ' ファイルオープン
    f.OpenSelectFile
    
    ' ファイルクローズ
    f.CloseFile
    
    Set f = Nothing
End Function

'**
'* Readメイン
'*
Function ReadMain() As Object
    Dim f As New File   ' ファイル
    Dim rng As Range    ' レンジオブジェクト
    Dim char As String  ' ファイルの1文字
    
    ' ファイルオープン
    f.OpenSelectFile
    
    ' 1文字を読み込む
    char = f.Read
    
    ' セルにファイルの1行を設定
    Set rng = Sheet1.Range(OUTPUT_POS)
    rng.Value = char
    
    ' ファイルクローズ
    f.CloseFile
    
    Set f = Nothing
    Set rng = Nothing
End Function

'**
'* Read Lileメイン
'*
Function ReadLineMain() As Object
    Dim f As New File   ' ファイル
    Dim rng As Range    ' レンジオブジェクト
    Dim line As String  ' ファイルの1行
    
    ' ファイルオープン
    f.OpenSelectFile
    
    ' 1行を読み込む
    line = f.ReadLine
    
    ' セルにファイルの1行を設定
    Set rng = Sheet1.Range(OUTPUT_POS)
    rng.Value = line
    
    ' ファイルクローズ
    f.CloseFile
    
    Set f = Nothing
    Set rng = Nothing
End Function

'**
'* Read Allメイン
'*
Function ReadAllMain() As Object
    Dim f As New File   ' ファイル
    Dim rng As Range    ' レンジオブジェクト
    Dim str As String   ' ファイルのすべての内容
    
    ' ファイルオープン
    f.OpenSelectFile
    
    ' 1行を読み込む
    str = f.ReadAll
    
    ' セルにファイルの1行を設定
    Set rng = Sheet1.Range(OUTPUT_POS)
    rng.Value = str
    
    ' ファイルクローズ
    f.CloseFile
    
    Set f = Nothing
    Set rng = Nothing
End Function

